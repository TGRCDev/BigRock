# BigRock

A 3D terrain library written in C++ (currently dependent on C++11), designed to be wrapped for any 3D game engine. It makes use of CSG primitives to place, remove and modify terrain in a sparse voxel octree. I plan to use multithreading and OpenCL to boost performance to run at real-time speeds on low-spec computers.

This library is currently incomplete, but is being worked on.

## Current Dependencies

- OpenGL Mathematics
    - <https://glm.g-truc.net/>
- FlatBuffers
    - <https://google.github.io/flatbuffers/>

![Prototype Image](https://tgrc.dev/wp-content/uploads/2019/10/preview.jpg)
